import numpy as np
from sklearn.tree import DecisionTreeClassifier
from sklearn.utils import shuffle
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.model_selection import train_test_split, cross_validate, GridSearchCV, validation_curve, learning_curve
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score, plot_confusion_matrix
from sklearn.datasets import fetch_openml
from sklearn.impute import SimpleImputer
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('ggplot')
from datetime import datetime

class Experiment():
    def __init__(self, leanerName, dataId, datasetName, pipe, gridParams,  kCVFolds=5, validationCurveParams=[], reshape=True, multiplot_params=None):
        self.leanerName = leanerName
        self.dataId = dataId
        self.datasetName = datasetName
        self.pipe = pipe
        self.gridParams = gridParams
        self.kCVFolds = kCVFolds
        self.validationCurveParams = validationCurveParams
        self.search = GridSearchCV(pipe, gridParams, n_jobs=32, pre_dispatch='2*n_jobs', cv=self.kCVFolds)
        self.reshape = reshape
        self.multiplot_params = multiplot_params
        
    def run(self):
        self.data = fetch_openml(data_id=self.dataId)
        X = self.data.data
        if self.reshape:
            y = self.data.target.reshape((-1,1))
        else:
            y = self.data.target.ravel()
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0, shuffle=True)
        self.X_train = X_train
        self.X_test = X_test
        self.y_train = y_train
        self.y_test = y_test
                
        search = self.search
        start = datetime.utcnow()
        search.fit(X_train, y_train)
        end = datetime.utcnow()
        print(f'{self.leanerName} on {self.datasetName} training took {end-start}')
        
        best_i = np.argmax(search.cv_results_['mean_test_score'])
        best_params = search.cv_results_["params"][best_i]
        self.pipe.set_params(**best_params)
        
        self.search_result = search.cv_results_
        self.best_params = best_params
        self.pipe.fit(X_train, y_train)
        self.test_score = self.pipe.score(X_test, y_test)
        print(f"{self.leanerName} on {self.datasetName} Test Score: {self.test_score}")
        
    def plotValidation(self):
        if(self.multiplot_params):
            self.plotValidationWithMultiPlot()
            return
        pipe = self.pipe
        pipe.set_params(**self.best_params)
        #colors = iter(['orangred', 'cyan', 'lime', 'orchid', 'yellow', 'm', 'y'])
        lineStyle = iter(['-','--','-.',':'])
        for param, paramName, _ in self.validationCurveParams:
            fig, ax = plt.subplots()
            ax.set_title(f'{self.datasetName}: {self.leanerName} - Learning Curve ({paramName})')
            val_train_scores, val_test_scores = validation_curve(pipe, self.X_train, self.y_train, 
                                                     param_name=param, 
                                                     param_range=self.gridParams[param])
            train_mean = np.mean(val_train_scores, axis=1)
            train_std = np.std(val_train_scores, axis=1)
            test_mean = np.mean(val_test_scores, axis=1)
            test_std = np.std(val_test_scores, axis=1)
            ax.plot(self.gridParams[param], train_mean, f'o-', label=f'Train score', alpha=0.6)
            #ax.fill_between(self.gridParams[param], train_mean - train_std, train_mean + train_std, alpha=0.1)
            ax.plot(self.gridParams[param], test_mean, f'x--', label=f'CV score', alpha=0.6)
            #ax.fill_between(self.gridParams[param], test_mean - test_std, test_mean + test_std, alpha=0.1)
            legend = ax.legend(loc=(1.04,0), shadow=True, fontsize='x-large')
            ax.set_xlabel(paramName)
            ax.set_ylabel('Accuracy')
            
            fig.text(0.45, 0.45, 'pcalderwood3', fontsize=25, color='gray', alpha=0.5)
        plt.show()
    
    def plotValidationWithMultiPlot(self):
        pipe = self.pipe
        pipe.set_params(**self.best_params)
        colors = iter(['#1f77b4', '#ff7e0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b'])
        lineStyle = ['-','--','-.',':','-','--','-.',':']
        for param, paramName, xscale in self.validationCurveParams:
            fig, ax = plt.subplots()
            ax.set_title(f'{self.datasetName}: {self.leanerName} - Learning Curve ({paramName})')
            ax.set_xscale(xscale)
            multiplot_param_name, mulitplot_param_vals = self.multiplot_params
            for j, mulitplot_val in enumerate(mulitplot_param_vals):
                c = next(colors)
                is_best = mulitplot_val == self.best_params[multiplot_param_name]
                pipe.set_params(**{multiplot_param_name: mulitplot_val}) #e.g. knn__metric: manhattan
                val_train_scores, val_test_scores = validation_curve(pipe, self.X_train, self.y_train, 
                                                         param_name=param, 
                                                         param_range=self.gridParams[param])
                train_mean = np.mean(val_train_scores, axis=1)
                train_std = np.std(val_train_scores, axis=1)
                test_mean = np.mean(val_test_scores, axis=1)
                test_std = np.std(val_test_scores, axis=1)
                ax.plot(self.gridParams[param], train_mean, f'o{lineStyle[0]}', color=c, label=f'{mulitplot_val} Train score{" (Best)" if is_best else ""}', alpha=0.6, linewidth=j+2, markersize=j+5)
                #ax.fill_between(self.gridParams[param], train_mean - train_std, train_mean + train_std, alpha=0.1)
                ax.plot(self.gridParams[param], test_mean, f'x{lineStyle[1]}', color=c, label=f'{mulitplot_val} CV score{" (Best)" if is_best else ""}', alpha=0.6, linewidth=j+2, markersize=j+5)
                #ax.fill_between(self.gridParams[param], test_mean - test_std, test_mean + test_std, alpha=0.1)
                legend = ax.legend(loc=(1.04,0), shadow=True, fontsize='x-large')
                ax.set_xlabel(paramName)
                ax.set_ylabel('Accuracy')
            fig.text(0.45, 0.45, 'pcalderwood3', fontsize=25, color='gray', alpha=0.5)
        plt.show()
    
    def plotConfusion(self):
        pipe = self.pipe
        pipe.set_params(**self.best_params)
        estimator = pipe.fit(self.X_train, self.y_train)
            #y_test_pred = estimator.predict(X_test)
        fig, ax = plt.subplots()
        fig.text(0.45, 0.45, 'pcalderwood3', fontsize=25, color='gray', alpha=0.5)
        disp = plot_confusion_matrix(estimator, self.X_test, self.y_test, cmap='GnBu', ax=ax)
        disp.ax_.set_title(f'{self.datasetName}: {self.leanerName} - Confusion Matrix')
        fig, ax = plt.subplots()
        fig.text(0.45, 0.45, 'pcalderwood3', fontsize=25, color='gray', alpha=0.5)
        disp = plot_confusion_matrix(estimator, self.X_test, self.y_test, normalize='true', values_format='.2f', cmap='GnBu', ax=ax)
        disp.ax_.set_title(f'{self.datasetName}: {self.leanerName} - Confusion Matrix (normalized)')
        plt.show()
        
    def topParams(self, n=10):
        indices = np.argpartition(self.search_result['mean_test_score'], -n)[-n:]
        indices = indices[np.argsort(self.search_result['mean_test_score'][indices])][::-1]
        topN = [(self.search_result['mean_test_score'][i], self.search_result['params'][i]) for i in indices]
        return topN
    
    def plotLearningCurve(self):
        pipe = self.pipe
        pipe.set_params(**self.best_params)
        space = np.linspace(0.25, 1.0, 15)
        train_sizes, train_scores, test_scores, fit_times, score_times = learning_curve(pipe, self.X_train, self.y_train, train_sizes=space, cv=self.kCVFolds, return_times=True)

        train_scores_mean = np.mean(train_scores, axis=1)
        train_scores_std = np.std(train_scores, axis=1)
        test_scores_mean = np.mean(test_scores, axis=1)
        test_scores_std = np.std(test_scores, axis=1)
        fit_times_mean = np.mean(fit_times, axis=1)
        fit_times_std = np.std(fit_times, axis=1)


        fig, ax = plt.subplots()
        fig.text(0.45, 0.45, 'pcalderwood3', fontsize=25, color='gray', alpha=0.5)
        ax.set_title(f'{self.datasetName}: {self.leanerName} - Learning Curve')
        ax.plot(train_sizes, train_scores_mean,  'o-', label='Train score')
        ax.fill_between(train_sizes, train_scores_mean - train_scores_std, train_scores_mean + train_scores_std, alpha=0.3)
        ax.plot(train_sizes, test_scores_mean,  'x-', label='CV score')
        ax.fill_between(train_sizes, test_scores_mean - test_scores_std, test_scores_mean + test_scores_std, alpha=0.3)
        ax.grid(True)

        ax2 = ax.twiny()
        ax1ticks = ax.get_xticks()
        percentage_ticks = []
        for x in ax1ticks:
            percentage_ticks.append(int(100*x/len(self.y_train)))

        ax2.set_xticks(ax1ticks)
        ax2.set_xbound(ax.get_xbound())
        ax2.set_xticklabels(percentage_ticks)    

        legend = ax.legend(loc=(1.04,0), shadow=True, fontsize='x-large')
        ax2.set_xlabel('% of Training Data')
        ax.set_xlabel('Training Samples')
        ax.set_ylabel('Accuracy')

        # Time plots
        mean_fit_times = np.mean(fit_times, axis=1)
        std_fit_times = np.std(fit_times, axis=1)
        fig, ax = plt.subplots()
        fig.text(0.45, 0.45, 'pcalderwood3', fontsize=25, color='gray', alpha=0.5)
        ax.set_title(f'{self.datasetName}: {self.leanerName} - Learning Curve')
        ax.plot(train_sizes, mean_fit_times,  'o-', label='Train time')
        ax.fill_between(train_sizes, mean_fit_times - std_fit_times, mean_fit_times + std_fit_times, alpha=0.3)

        mean_score_times = np.mean(score_times, axis=1)
        std_score_times = np.std(score_times, axis=1)
        ax.plot(train_sizes, mean_score_times,  'x-', label='CV')
        ax.fill_between(train_sizes, mean_score_times - std_score_times, mean_score_times + std_score_times, alpha=0.3)
        ax.grid(True)

        ax2 = ax.twiny()
        ax1ticks = ax.get_xticks()
        percentage_ticks = []
        for x in ax1ticks:
            percentage_ticks.append(int(100*x/len(self.y_train)))

        ax2.set_xticks(ax1ticks)
        ax2.set_xbound(ax.get_xbound())
        ax2.set_xticklabels(percentage_ticks)    

        legend = ax.legend(loc=(1.04,0), shadow=True, fontsize='x-large')
        ax2.set_xlabel('% of Training Data')
        ax.set_xlabel('Training Samples')
        ax.set_ylabel('Time (s)')

        plt.show()

    def measureFitTime(self):
        pipe = self.pipe
        pipe.set_params(**self.best_params)
        start = datetime.utcnow()
        pipe.fit(self.X_train, self.y_train)
        end = datetime.utcnow()
        print(f'Best parameters took {end-start} to train')
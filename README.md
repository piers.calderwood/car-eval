# CS7641 Analysis

Exploration of the [SpeedDating](https://www.openml.org/d/40536) and [PenDigits](https://www.openml.org/d/32) datasets

Code is found in this repo:
https://gitlab.com/piers.calderwood/car-eval/

The specific notebook to generate all graphs can be found here:
https://gitlab.com/piers.calderwood/car-eval/-/blob/master/00.RunMe.ipynb

How to run:
1. Create python3 virtual environment in the root git directory
    - python version 3.8.2 was used during development
2. Source the new virtual environement
    - source ./venv/bin/activate
3. Install requirements specified in `requirements.txt`
    - `$ pip install requirements.txt`
4. Start the jupyter notebook server
    - `$ jupyter`
5. Open the `00.RunMe` notebook and execute all cells in order
    - The experiments are broken up into seperate notebooks, one per estimator
    - This notebook will output all of the results
    - Some additional graphs are create after all experimentation completes, later cells.
6. Alternatively, each estimator's experiments can be run individually by opening the associated notebook and running all cells
    - The cached version of the individual notebook might not be up to date with the output from the "all-in-one"